module multiplicador 
    #(parameter SIZE = 16)
    (  
    //outputs
    output wire [2*SIZE-1:0] data_out,  
    //inputs
    input [SIZE-1:0] inA,
    input [SIZE-1:0] inB
    );

    //parameter max = SIZE - 1;

    wire [SIZE-1:0] temp [SIZE-1:0];
    wire [SIZE-1:0] out_t [SIZE-1:0];
    
    

    generate
        genvar filas,columnas;
        for(filas=0; filas<SIZE; filas=filas+1) begin : rows
            for(columnas=0; columnas<SIZE; columnas=columnas+1) begin : col 
                if (filas==0)begin
                    if (columnas==0)begin
                        sumador sum(
                        //entradas
                        .inA (inA[columnas]&inB[filas]),
                        .inB (0),
                        .carry (0),
                        //salidas
                        .out_Sum (data_out[filas+columnas]),
                        .out_Carry (temp[filas][columnas])
                        );
                    end

                    else if (columnas!=0 && columnas!=(SIZE-1)) begin
                        sumador sum(
                        //entradas
                        .inA (inA[columnas]&inB[filas]),
                        .inB (0),
                        .carry (temp[filas][columnas-1]),
                        //salidas
                        .out_Sum (out_t[filas][columnas-1]),
                        .out_Carry (temp[filas][columnas])
                        );        
                    end
                    else if (columnas==(SIZE-1)) begin
                        sumador sum(
                        //entradas
                        .inA (inA[columnas]&inB[filas]),
                        .inB (0),
                        .carry (temp[filas][columnas-1]),
                        //salidas
                        .out_Sum (out_t[filas][columnas-1]),
                        .out_Carry (temp[filas][columnas])
                        );     
                    end
                end

                else if(filas!=0 && filas!=(SIZE-1)) begin
                    if (columnas==0)begin
                        sumador sum(
                        //entradas
                        .inA (inA[columnas]&inB[filas]),
                        .inB (out_t[filas-1][columnas]),
                        .carry (0),
                        //salidas
                        .out_Sum (data_out[filas+columnas]),
                        .out_Carry (temp[filas][columnas])
                        );
                    end
                    else if (columnas!=0 && columnas!=(SIZE-1)) begin
                        sumador sum(
                        //entradas
                        .inA (inA[columnas]&inB[filas]),
                        .inB (out_t[filas-1][columnas]),
                        .carry (temp[filas][columnas-1]),
                        //salidas
                        .out_Sum (out_t[filas][columnas-1]),
                        .out_Carry (temp[filas][columnas])
                        );        
                    end
                    else if (columnas==(SIZE-1)) begin
                        sumador sum(
                        //entradas
                        .inA (inA[columnas]&inB[filas]),
                        .inB (temp[filas-1][columnas]),
                        .carry (temp[filas][columnas-1]),
                        //salidas
                        .out_Sum (out_t[filas][columnas-1]),
                        .out_Carry (temp[filas][columnas])
                        );     
                    end    
                end
                else if (filas==(SIZE-1)) begin
                    if (columnas==0)begin
                        sumador sum(
                        //entradas
                        .inA (inA[columnas]&inB[filas]),
                        .inB (out_t[filas-1][columnas]),
                        .carry (0),
                        //salidas
                        .out_Sum (data_out[filas+columnas]),
                        .out_Carry (temp[filas][columnas])
                        );
                    end
                    else if (columnas!=0 && columnas!=(SIZE-1)) begin
                        sumador sum(
                        //entradas
                        .inA (inA[columnas]&inB[filas]),
                        .inB (out_t[filas-1][columnas]),
                        .carry (temp[filas][columnas-1]),
                        //salidas
                        .out_Sum (data_out[filas+columnas]),
                        .out_Carry (temp[filas][columnas])
                        );        
                    end
                    else if (columnas==(SIZE-1)) begin
                        sumador sum(
                        //entradas
                        .inA (inA[columnas]&inB[filas]),
                        .inB (temp[filas-1][columnas]),
                        .carry (temp[filas][columnas-1]),
                        //salidas
                        .out_Sum (data_out[filas+columnas]),
                        .out_Carry (data_out[filas+columnas+1])
                        );     
                    end   
                end
            end
        end        
    endgenerate
endmodule       