`timescale 1 ns / 1 ps

module system (
	input            clk,
	input            resetn,
	input 	[3:0]	 BTN,
	output           trap,
	output wire HS,
	output wire VS,
	output wire [3:0] R,
	output wire [3:0] G,
	output wire [3:0] B,
	output reg [2:0] leds
); 
  
 
 
 
 
    reg [31:0] out_byte;
    reg out_byte_en;
	// set this to 0 for better timing but less performance/MHz
	parameter FAST_MEMORY = 1;
    wire clock;
	// 4096 32bit words = 16kB memory
//	parameter MEM_SIZE = 4096;
	parameter MEM_SIZE = 16384;
	wire mem_valid;
	wire mem_instr;
	reg mem_ready;
	wire [31:0] mem_addr;
	wire [31:0] mem_wdata;
	wire [3:0] mem_wstrb;
	reg [31:0] mem_rdata;
	
	wire mem_la_read;
	wire mem_la_write;
	wire [31:0] mem_la_addr;
	wire [31:0] mem_la_wdata;
	wire [3:0] mem_la_wstrb;

    //-------
    reg [15:0] in_mulA;
    reg [15:0] in_mulB;    
    reg        ini;
    reg [15:0] in_fact;
    wire [15:0] out_fact;
    wire       fact_done;
    wire [31:0] out_mul;
	wire [31:0] out_mux;
	reg display;
	wire cpu_valid;
	wire cpu_instr;
	wire cpu_ready;
	wire [31:0] cpu_addr;
	wire [31:0] cpu_wdata;
	wire [3:0] cpu_wstrb;
	wire [31:0] cpu_rdata;
	wire [3:0] state;
	wire [3:0] next_state;
	reg awk;
	wire boton_izq;
	wire boton_der;
	

picorv32 #(.ENABLE_MUL(1)) 
picorv32_core (
		.clk         (clk         ),
		.resetn      (resetn      ),
		.trap        (trap        ),
		.mem_valid   (mem_valid   ),
		.mem_instr   (mem_instr   ),
		.mem_ready   (mem_ready   ),
		.mem_addr    (mem_addr    ),
		.mem_wdata   (mem_wdata   ),
		.mem_wstrb   (mem_wstrb   ),
		.mem_rdata   (mem_rdata   ),
		.mem_la_read (mem_la_read ),
		.mem_la_write(mem_la_write),
		.mem_la_addr (mem_la_addr ),
		.mem_la_wdata(mem_la_wdata),
		.mem_la_wstrb(mem_la_wstrb)
	);
//wire clock;
reg w_ram;
reg [19:0] r_ram;
reg [19:0] wa_ram;
reg [11:0] in_ram ;
wire [11:0] out_ram;
wire [20:0] add_color;
reg controller_reset = 0;
wire [20:0] CurrentCol;
wire [20:0] wCurrentRow;
wire [11:0] out_vga;
wire iniDisp;
wire [9:0] Counterx;
wire [8:0] Countery;

hvsync_generator vga2( 
        .clk (clk),
        .resetn (resetn),
        .color (out_ram),
        .vga_h_sync (HS),
        .vga_v_sync (VS),
        .inDisplayArea (iniDisp),
        .CounterX (Counterx),
        .CounterY (Countery),
        .clock (clock),
        .add_color (add_color),
        .vga_out (out_vga)
        );

wire [2:0] column_loc;
wire enter_flag;
control botones(
		.clk (clk),
		.resetn (resetn),
		.awk_in (awk),
		.bot_in (BTN),
		.xloc (column_loc),
		.boton_der (boton_der),
		.boton_izq (boton_izq),
		.enter (enter_flag)
		);		



assign R = out_vga[11:8];
assign G = out_vga[7:4];
assign B = out_vga[3:0];
ram raml(
        .Clock (clk),
        .iWriteEnable (w_ram),
        .iReadAddress (add_color),
        .iWriteAddress (wa_ram),
        .iDataIn (in_ram),
        .oDataOut (out_ram)
        );


	
	reg [31:0] memory [0:MEM_SIZE-1];

`ifdef SYNTHESIS
    initial $readmemh("../firmware/firmware.hex", memory);
`else
	initial $readmemh("firmware.hex", memory);
`endif

	reg [31:0] m_read_data;
	reg m_read_en;

	generate if (FAST_MEMORY) begin
		always @(posedge clk) begin
			mem_ready <= 1;
			out_byte_en <= 0;
			awk <= 0;
			mem_rdata <= memory[mem_la_addr >> 2];
			if (mem_la_write && (mem_la_addr >> 2) < MEM_SIZE) begin
				if (mem_la_wstrb[0]) memory[mem_la_addr >> 2][ 7: 0] <= mem_la_wdata[ 7: 0];
				if (mem_la_wstrb[1]) memory[mem_la_addr >> 2][15: 8] <= mem_la_wdata[15: 8];
				if (mem_la_wstrb[2]) memory[mem_la_addr >> 2][23:16] <= mem_la_wdata[23:16];
				if (mem_la_wstrb[3]) memory[mem_la_addr >> 2][31:24] <= mem_la_wdata[31:24];
			end
			else
			if (mem_la_write && mem_la_addr == 32'h1000_0004) begin
				w_ram <= 1;	
				wa_ram <= mem_la_wdata;
				r_ram <= mem_la_wdata;			
			end
			if (mem_la_write && mem_la_addr == 32'h1000_0000) begin
			    in_ram <= mem_la_wdata;
			    w_ram <= 1;			
			end
			if (mem_la_write && mem_la_addr == 32'h1000_0008) begin
			    //wa_ram <= mem_la_wdata;
			    w_ram <= 0;	
			    		
			end
		
			if (mem_la_read && mem_la_addr == 32'h1000_0018) begin
			    //wa_ram <= mem_la_wdata;
			    mem_rdata <= boton_izq;
			    if(boton_izq) awk <= 1;
			   
			  
			 
			 
			    		
			end
			if (mem_la_read && mem_la_addr == 32'h1000_0010) begin
			    //wa_ram <= mem_la_wdata;
			    mem_rdata <= boton_der;
			    if(boton_der) awk <= 1;
			    		
			end
			
			
			//ifs para game logic
			if ( mem_la_read && mem_la_addr == 32'h1000_0014) begin
			     mem_rdata <= enter_flag;
			     if(enter_flag) awk <= 1;
			end

			if ( mem_la_read && mem_la_addr == 32'h1000_000A) begin
			     mem_rdata <= enter_flag;
			     if(enter_flag) awk <= 1;
			     
			end

			
			
			
		end
	end else begin
		always @(posedge clk) begin
			m_read_en <= 0;
			mem_ready <= mem_valid && !mem_ready && m_read_en;

			m_read_data <= memory[mem_addr >> 2];
			mem_rdata <= m_read_data;

			out_byte_en <= 0;

			(* parallel_case *)
			case (1)
				mem_valid && !mem_ready && !mem_wstrb && (mem_addr >> 2) < MEM_SIZE: begin
					m_read_en <= 1;
				end
				mem_valid && !mem_ready && |mem_wstrb && (mem_addr >> 2) < MEM_SIZE: begin
					if (mem_wstrb[0]) memory[mem_addr >> 2][ 7: 0] <= mem_wdata[ 7: 0];
					if (mem_wstrb[1]) memory[mem_addr >> 2][15: 8] <= mem_wdata[15: 8];
					if (mem_wstrb[2]) memory[mem_addr >> 2][23:16] <= mem_wdata[23:16];
					if (mem_wstrb[3]) memory[mem_addr >> 2][31:24] <= mem_wdata[31:24];
					mem_ready <= 1;
				end
				
			endcase
		end
	end endgenerate
endmodule

