module bounce(
    input resetn,
    input clk,
    input [3:0] bot_in,
    input awk,
    output reg [3:0] bot_out
    );
 
    reg [32:0] filter_counter;
    reg [3:0] temp_bot;
    reg bounce_flag;
 
 
    always @( posedge clk) begin
        if(~resetn) begin
            filter_counter <= 0;
            bounce_flag <= 0;
            temp_bot <=0;
            bot_out <= 0;
        end else begin
            if(awk) begin
                bot_out <=0;
                temp_bot <=0;
            end
            if(bot_in) begin
                bounce_flag <= 1;
                temp_bot <= bot_in;
            end 
            if(bounce_flag) begin
                filter_counter <= filter_counter + 1;
                
                
                
                
            end
            if(filter_counter == 23000000) begin
                bot_out <= temp_bot;
                filter_counter <= 0;
                bounce_flag <= 0;
            end
        end
    end
 

endmodule // bounce
