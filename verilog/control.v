module control(
    input clk,
    input resetn,
    input awk_in,
    input [3:0] bot_in,
    output reg [2:0] xloc,
    output reg boton_der,
    output reg boton_izq,
    output reg enter 
);


wire [3:0] bot_c; //0 a 7
reg [13:0] ctr_pog; //100 MHz => 10ns, counter hasta 100 us => 10000 clk cycles
reg awk;
bounce filter(
    .resetn (resetn),
    .clk    (clk),
    .awk    (awk),
    .bot_in (bot_in),
    .bot_out (bot_c)
);

//bot_c[0]=boton izquierdo
//bot_c[1]=boton derecho
//bot_c[3]=boton central



always @(posedge clk) begin
    if(~resetn) begin
        xloc <= 0;
        enter <= 0;
        ctr_pog <= 0;
        awk <=0;
        boton_izq <= 0;
        boton_der <= 0;
    end else begin
        awk<=0;
        if(awk_in) begin
            boton_izq <= 0;
            boton_der <= 0;
            enter <= 0;
        end
        if(bot_c[0] != 0 ) begin //si se presiona el boton izquierdo se puede decrementar el contador de columna siempre y cuando el contador no sea 0
            xloc <= xloc - 1;
            boton_izq <= 1;
            awk <= 1;
        end 
        
        if(bot_c[1] != 0) begin //si se presiona el boton derecho se puede incrementar el contador de columna siempre y cuando el contador no sea 6
            xloc <= xloc + 1;
            boton_der <= 1;
            awk<=1;
        end
        if (bot_c[3] != 0) begin //si se presiona el boton central se indica
            enter <= 1;
            awk<=1;
        end
        if (ctr_pog==10000) begin
            //enter <= 0;
            ctr_pog <= 0;
        end
        if (ctr_pog!=10000 && enter) begin
            ctr_pog <= ctr_pog + 1;
        end
    end
end

endmodule // control