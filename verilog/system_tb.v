`timescale 1 ns / 1 ps

module system_tb;
	reg clk = 1;
	reg [20:0] counter = 0;
	always #5 clk = ~clk;
    integer fil;
	reg resetn = 0;
	reg [3:0] BTN; 
	initial begin
		fil = $fopen("/home/alberto/Desktop/frames.txt","w");
		if ($test$plusargs("vcd")) begin
			$dumpfile("system.vcd");
			$dumpvars(0, system_tb);
		end
		repeat (2) @(posedge clk);
		resetn <= 1;
		$timeformat(-9,0," ns");
		BTN = 'b1000; 
		repeat (10) @(posedge clk);
		BTN = 0;
		
	end

	wire trap;
	wire [3:0] R;
	wire [3:0] G;
	wire [3:0] B;
	wire HS;
	wire VS;
	wire clock;
	wire [2:0] leds;

	system uut (
		.clk        (clk        ),
		.resetn     (resetn     ),
		.BTN        (BTN),
		.trap       (trap       ),
		.HS         (HS),
		.VS         (VS),
		.R          (R),
		.G          (G),
		.B          (B),
        .leds       (leds)
	);

	always @(posedge clock) begin
		counter <= counter + 1; 
		if (counter<2000000) begin
			//if (!fil)
			//$display("no se abrio");
			$timeformat(-9,0," ns");
			$fwrite(fil,"%t: %b %b %b %b %b\n",$time,HS, VS, R, G, B);
		end
		else begin
			//$fdisplay(fil, "test");	
			
		//end	
		//if (resetn)
			$fclose(fil);
			$finish;

		end
	end
endmodule
