module VGA_controller (
	input clk,
	input resetn,
	input [11:0] color,
	input prom_flag,
	output reg [11:0] vga_out,
	output reg HOR_SYNC,
	output reg VERT_SYNC,
	output reg clock,
	output reg [19:0] add_color,
	output pixel,
	output linea
    );

reg [15:0] HOR_COUNTER;
reg [31:0] VERT_COUNTER; 
reg [2:0]  divider_counter;
reg [20:0] pixel;
reg [20:0] linea; 
reg 	   reset_2;
reg        reset_3;
reg [3:0] prom;

	
	//Divisor de frecuencia de reloj.
	always @(posedge clk) begin
		if(~resetn) begin
			divider_counter <= 0;
			clock <= 0;
			reset_2 <= 0;
			reset_3 <= 0;
			
		end else begin
			divider_counter <= divider_counter + 1;
			if(divider_counter == 1) begin
				divider_counter <= 0;
				clock <= ~clock;
				reset_2 <= resetn;
				reset_3 <= reset_2;
			end
		end
	end
	//Se utiliza el reloj de 25MHz
	always @(posedge clock) begin
		if(~reset_3) begin
			HOR_COUNTER <= 0;
			VERT_COUNTER <= 0;
			linea <= 0;
			pixel <= 0;
		end else begin
		    add_color <= linea+ pixel + (639*linea);
		    if(HOR_COUNTER <96) begin
		      HOR_SYNC <=0;
		    end
		    if(HOR_COUNTER >= 96) begin
			     HOR_SYNC <= 1;
            end 
		    if(HOR_COUNTER >= 144 && HOR_COUNTER < 784) begin
			     HOR_SYNC <= 1;
			     vga_out <= 'h0F0;
		    end
			if(HOR_COUNTER == 800) begin
				HOR_COUNTER <= 0;
				linea <= linea + 1;
				pixel<=0;
				//VERT_COUNTER <= VERT_COUNTER + 1;
			end else begin
				HOR_COUNTER = HOR_COUNTER + 1;
			end
			if (HOR_COUNTER > 144 && HOR_COUNTER < 784) begin
			    pixel <= pixel + 1;
			    if(pixel == 640) begin
			         pixel <= 0;
			    end
			end
			if(VERT_COUNTER <1600) begin
			 VERT_SYNC<=0;
			end
			if(VERT_COUNTER >= 1600) begin
			VERT_SYNC = 1;
		end 
		if(VERT_COUNTER >= 24800) begin
			VERT_SYNC = 1;
		end
			if(linea==521) begin
				    linea <= 0;
			end
			
			if(VERT_COUNTER == 416800) begin
				VERT_COUNTER <= 0;
				
			end else begin
				VERT_COUNTER = VERT_COUNTER + 1;
				
			end
		end
	end
	//Horizontal sync logic. 
	always @(HOR_COUNTER) begin
		


	end
	//Vertical sync logic. 
	always @(VERT_COUNTER) begin
		
	end

endmodule  