module factorial (
    input clk,
    input reset,
    input ini,
    input [15:0] fact,
    output reg [31:0] out_result,
    output reg done
);

    reg started_flag;
    reg [31:0] result_anterior;
    reg [15:0]  factor;
    reg [31:0] total_actual;
    wire [31:0] total_rn;
    multiplicador #(.SIZE(16)) multi (
        .data_out   (total_rn),
        .inA        (fact),
        .inB        (factor)
    );

    always @(posedge clk ) begin
        if(~reset) begin
            done <= 0;
            total_actual <= 0;
            out_result <= 0;
            factor <= 1;
            result_anterior <= 0;
            started_flag <= 0;
        end else begin
            if(ini && started_flag == 0) begin
                started_flag <= 1;
                factor <= fact;
                total_actual <= 0;
                done <= 0;
           end else begin
                if(started_flag) begin
                    if(factor == 0) begin
                        factor <= 0;
                        started_flag <= 0;
                        out_result <= total_actual;
                        done <= 1;
                        
                    end else factor <= factor -1;
                    result_anterior <= total_rn;
                end
                total_actual <= result_anterior + total_actual;
           end
                
            
        end
    end














endmodule