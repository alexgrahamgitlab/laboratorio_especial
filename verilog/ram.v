module ram # ( parameter DATA_WIDTH= 12,
parameter ADDR_WIDTH=19, parameter MEM_SIZE=640*480 )
(
input wire Clock,
input wire iWriteEnable,
input wire[ADDR_WIDTH-1:0] iReadAddress,
input wire[ADDR_WIDTH-1:0] iWriteAddress,
input wire[DATA_WIDTH-1:0] iDataIn,
output reg [DATA_WIDTH-1:0] oDataOut
);
    reg [DATA_WIDTH-1:0] Ram [MEM_SIZE:0];
    always @(posedge Clock)
    begin
        if (iWriteEnable) begin
            Ram[iWriteAddress] <= iDataIn;
            oDataOut <= Ram[iReadAddress];
        end
        else begin
            oDataOut <= Ram[iReadAddress];
        end
    end
endmodule