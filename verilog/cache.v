
module cache #(parameter WAYS = 2, OFFSET= 2, WORD_SIZE = 32, CACHE_SIZE = 1024) (
    input clk,
    input resetn,
    input cpu_valid,
    input cpu_instr,
    input [31:0] cpu_addr,
    input [WORD_SIZE-1:0] cpu_wdata,
    input [3:0] cpu_strb,
    input mem_ready,
    input [31:0] mem_rdata,
    output reg cpu_ready,
    output reg [WORD_SIZE-1:0] cpu_rdata,
    output reg mem_valid,
    output reg mem_instr,
    output reg [WORD_SIZE-1:0] mem_addr,
    output reg [WORD_SIZE-1:0] mem_wdata,
    output reg [3:0] mem_strb,
    output reg [3:0] state,
    output reg [3:0] next_state
);

//Declaracion de Estados de la FSM 
localparam  IDLE = 'h0;
localparam  DECODE = 'h1;
localparam  CHECK = 'h3;
localparam  MISS ='h5;
localparam  HIT = 'h6;
localparam  SEND_DATA = 'hB;
localparam  WRITE_BACK = 'hC;
localparam  GET_DATA = 'hD;
localparam  WRITE_DATA = 'h9;
localparam  LRU_UPDATE = 'hA;
localparam  VICTIMIZE= 'h7;
localparam  DONE ='h8;
localparam  SAVE ='hE;
   
//FSM 
//reg [3:0] state;
//reg [3:0] next_state; 

//Formulas 
localparam CACHE_ENTRY = CACHE_SIZE/4;
localparam INDEX = $clog2(CACHE_ENTRY/WAYS);
localparam TAG = WORD_SIZE - INDEX - OFFSET;

//Contadores

reg [63:0] HITS = 0;
reg [63:0] MISSES = 0;

//Cosas para el cache
reg [WORD_SIZE-1:0] cache_data [CACHE_ENTRY-1:0];
reg [CACHE_ENTRY-1:0] cache_valid = 0;
reg [CACHE_ENTRY-1:0] cache_lru = 0;
reg [TAG-1:0] cache_tag [CACHE_ENTRY-1:0];

reg [1:0] HIT_LOCATION;
reg [WAYS-1:0] WAY_TO_WRITE;
reg [31:0] data_writeback;
reg flag; 
reg [WAYS-1:0] SEARCH_COUNTER;
reg FIND_FLAG;
reg [TAG-1:0] tag_temp;
reg [INDEX-1:0] index_temp;
reg [TAG-1:0]   tag_in;
reg [INDEX-1:0] index;
reg [INDEX+1:0] index_in;
reg [1:0] TEMP_HIT_LOC;
reg [31:0] temp_data_writeback;

reg [31:0] cpu_addr_s;
integer i;

    always @(posedge clk) begin  // always block to update state
            if (~resetn)begin
                state <= IDLE;
                SEARCH_COUNTER <= 0;
            end
            else begin
                state <= next_state; 
                tag_temp <= tag_in;
                index_temp <= index_in;
                TEMP_HIT_LOC <= HIT_LOCATION;
                temp_data_writeback <= data_writeback;
                if (MISS) begin
                    SEARCH_COUNTER <= SEARCH_COUNTER + 1;

                end
            end
        end
    
    always @(*) begin //Logica de salidas. 
        case(state) 
        IDLE: begin
            cpu_ready = 0;
            mem_valid = 0;
            FIND_FLAG = 0; 
        end
        DECODE: begin
            cpu_ready = 0;
            index = cpu_addr[INDEX+1:2];
            tag_in = cpu_addr[31:INDEX+2]; //Guarda pedazo del tag del address.
            index_in = 2*cpu_addr[INDEX+1:2]; // guarda pedazo del index_temp del address
        end
        HIT: begin          
            if (flag == 1) begin
                cpu_rdata = cache_data[index_in + HIT_LOCATION];
            end
            if (cpu_strb == 'h0) begin
                cpu_rdata = cache_data[index_in + HIT_LOCATION];
            end
        end
        MISS: begin
            
        end
        GET_DATA: begin
            mem_addr = cpu_addr_s;
            mem_valid = 1;
            mem_strb = 0;
            mem_instr = cpu_instr;
            if (mem_ready) begin
                cache_data[index_in +WAY_TO_WRITE] = mem_rdata;
                cache_valid[index_in + WAY_TO_WRITE] = 1;
                cache_tag[index_in+ WAY_TO_WRITE] = tag_in;
                cpu_rdata = cache_data[index_in + WAY_TO_WRITE];
                mem_valid = 0;
            end else begin
                
            end
        end
        LRU_UPDATE: begin
            
        end
        WRITE_BACK: begin
            //mem_addr = cpu_addr_s;
            mem_addr[31:INDEX+2] = cache_tag[index_in+ WAY_TO_WRITE];
            mem_addr[INDEX+1:2] = index_in/2;
            mem_addr[1:0] = 0;
            mem_valid = 1;
            mem_strb = 'hf;
            mem_wdata = data_writeback;
            if(mem_ready) begin
                mem_valid = 0;
            end
        end
        SAVE: begin
            mem_strb = 0;
        end
        DONE: begin
            mem_valid = 0;
            cpu_ready = 0;        
        end 
            
        endcase 
    end


    always @(state or SEARCH_COUNTER) begin  //Logica de next_state
        next_state=state;
        
        case(state)
        IDLE: begin
        //quite la condicion del strobe;
            if(cpu_valid==1 & cpu_instr==1 ) begin
                 next_state = DECODE;
                 flag = 1; //INDICA READ
            end 
            else begin
                if(cpu_valid && ~cpu_instr ) begin
                    next_state = DECODE;
                    flag = 0; //INDICA WRITE
                end else begin
                    next_state = IDLE;
                end
            end
        end
        DECODE: begin
            cpu_addr_s = cpu_addr;
            SEARCH_COUNTER = 0;
            next_state = CHECK; 
        end

        CHECK: begin
            if (SEARCH_COUNTER  - 1< WAYS) begin
                if(cache_tag[index_in+SEARCH_COUNTER-1] == tag_in) begin
                    HIT_LOCATION = SEARCH_COUNTER-1;
                    SEARCH_COUNTER = 0;
                    HITS = HITS + 1;
                    next_state = HIT;
                    
                end 
            end else begin 
                SEARCH_COUNTER  = 0;
                MISSES = MISSES + 1;
                next_state = MISS;
                
            end  
            //next_state = CHECK;
        end
        HIT: begin
            next_state = LRU_UPDATE; 
        end
        MISS: begin
            if(SEARCH_COUNTER - 1 < WAYS) begin
                if(cache_valid[index_in + SEARCH_COUNTER - 1] == 0) begin
                    //if(flag == 1 && cpu_strb == 0) begin
                    if (flag ==1) begin
                        HIT_LOCATION = SEARCH_COUNTER-1;
                        WAY_TO_WRITE = HIT_LOCATION;
                        next_state = GET_DATA;
                        
                    end else begin
                        HIT_LOCATION = SEARCH_COUNTER - 1;
                        cache_valid[index_in + SEARCH_COUNTER -1 ] = 1;
                        cache_tag[index_in + SEARCH_COUNTER-1] = tag_in;
                        cache_data[index_in + SEARCH_COUNTER - 1] = cpu_wdata;
                        next_state = LRU_UPDATE;
                    end
                end
            end else begin
                next_state = VICTIMIZE;
            end
            //next_state = MISS;
        end
        GET_DATA: begin
            if(mem_ready) begin
                
                HIT_LOCATION = WAY_TO_WRITE; 
                //if(FIND_FLAG == 1) next_state = WRITE_BACK;
                next_state = LRU_UPDATE;
                
            end else next_state = GET_DATA;
        end
        LRU_UPDATE: begin
            cpu_ready = 1;
            if(HIT_LOCATION == 0 ) begin
                cache_lru[index_in] = 0;
                cache_lru[index_in+1] = 1;
            end else begin
                cache_lru[index_in] = 1;
                cache_lru[index_in+1] = 0;
            end
            next_state = DONE; 
        end
        VICTIMIZE: begin
            FIND_FLAG = 1;
            if(cache_lru[index_in] == 1) begin
                data_writeback = cache_data[index_in];
                WAY_TO_WRITE = 0;
            end else begin
                data_writeback = cache_data[index_in+1];
                WAY_TO_WRITE = 1;
            end
            next_state = WRITE_BACK;
//            if(cpu_strb == 'hf) next_state = WRITE_BACK;
//            else next_state = GET_DATA;
        end
        WRITE_BACK: begin
            if(mem_ready) begin
                HIT_LOCATION = WAY_TO_WRITE;
                next_state = SAVE;
            end else next_state = WRITE_BACK;
        end
        SAVE: begin
            if(cpu_strb != 'hf) begin
                next_state = GET_DATA;
            end else begin
                cache_tag[index_in + WAY_TO_WRITE] = tag_in;
                cache_data[index_in + WAY_TO_WRITE] = cpu_wdata;
                next_state = LRU_UPDATE;
            end
        end
        DONE: begin
            next_state = IDLE; 
        end
        default: 
            next_state = IDLE; 
        endcase

    end


    
endmodule // cache
