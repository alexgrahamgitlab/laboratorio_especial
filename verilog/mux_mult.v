module mux_mult
  #(parameter size =16)
   (
    input [size-1:0] A,
    input [1:0] B,
    output reg  [2*size-1:0] out
    );

    always @(*) begin
        case(B) 
            2'b00:  out = 0;
            2'b01:  out = A;
            2'b10:  out = A << 1;
            2'b11:  out = A + (A << 1);
        endcase
    end

endmodule // mux_mult
