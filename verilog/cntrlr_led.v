module cntrlr_led (
    input [7:0] out_byte,
    input clk,
    input resetn,
    output reg [6:0] cathodes,
    output reg [7:0] anodes
);

    reg [19:0] counter;
    reg [3:0] value_display;
    wire [1:0] display_activate; //Indica que es momento de activar un display. 

    wire [11:0] bcd_val;
    bin_bcd convert (
        .bin (out_byte),
        .bcd (bcd_val)
    );

    assign display_activate = counter[19:18]; //Los dos ultimos bits del contador indican
                                              //el momento de inicial un display 

    always @(posedge clk) begin
        if(~resetn) begin
            counter <= 0;
        end else begin
            counter <= counter + 1; 
        end
    end
    
    always @(display_activate) begin
        case (display_activate) 
            'b00: begin
                anodes = 'b11111110;
                value_display = bcd_val[3:0];
            end
            'b01: begin
                anodes = 'b11111101;
                value_display = bcd_val[7:4];
            end
            'b10: begin
                anodes = 'b11111011;
                value_display = bcd_val[11:8];
            end
            'b11: begin
                anodes = 'b11110111;
                value_display = 'b0000;
            end
            default: begin
                anodes = 'b11111111;
                value_display = 'b0000;
            end
        endcase 
        case (value_display) //Case se encarga de decodificar de hexadecimal a 7seg.
            'b0000: begin
                cathodes = 7'b0000001; // '0'
            end 
            'b0001: begin
                cathodes = 7'b1001111; // '1'
            end
            'b0010: begin
                cathodes = 7'b0010010; // '2'
            end
            'b0011: begin
                cathodes = 7'b0000110; // '3'
            end
            'b0100: begin
                cathodes = 7'b1001100; // '4'
            end
            'b0101: begin
                cathodes = 7'b0100100; // '5'
            end
            'b0110: begin
                cathodes = 7'b0100000; // '6'
            end
            'b0111: begin
                cathodes = 7'b0001111; // '7'
            end
            'b1000: begin
                cathodes = 7'b0000000; // '8'
            end
            'b1001: begin
                cathodes = 7'b0000100; // '9'
            end
            default: cathodes = 7'b0000001;
        endcase
    end



endmodule // 
