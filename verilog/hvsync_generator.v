module hvsync_generator(
    input clk,
    input resetn,
    input [11:0] color,
    output vga_h_sync,
    output vga_v_sync,
    output reg inDisplayArea,
    output reg [9:0] CounterX,  //horizontal
    output reg [8:0] CounterY,  //vertical
    output reg clock,
    output reg [19:0] add_color,
    output reg [11:0] vga_out
  );
    reg vga_HS, vga_VS;
    reg reset_2,reset_3;
    reg [2:0]  divider_counter;

    //Divisor de frecuencia de reloj.
	always @(posedge clk) begin
		if(~resetn) begin
			clock <= 0;
            divider_counter <= 0;
            reset_2 <= 0;
            reset_3 <= 0; 
		end else begin
			divider_counter <= divider_counter + 1;
			if(divider_counter == 1) begin
				divider_counter <= 0;
				clock = ~clock;
                reset_2 <= resetn;
                reset_3 <= reset_2; 
			end
		end
	end

    wire CounterXmaxed = (CounterX == 800); // 16 + 48 + 96 + 640
    wire CounterYmaxed = (CounterY == 525); // 10 + 2 + 33 + 480


    
    always @(posedge clock) begin
        if (CounterXmaxed)
        CounterX <= 0;
        else
        CounterX <= CounterX + 1;
    end    

    always @(posedge clock)
    begin
      if (CounterXmaxed)
      begin
        if(CounterYmaxed)
          CounterY <= 0;
        else
          CounterY <= CounterY + 1;
      end
    end


    always @(posedge clock)
    begin
      vga_HS <= (CounterX > (640 + 16) && (CounterX < (640 + 16 + 96)));   // active for 96 clocks
      vga_VS <= (CounterY > (480 + 10) && (CounterY < (480 + 10 + 2)));   // active for 2 clocks
    end

    always @(posedge clock)
    begin
        inDisplayArea <= (CounterX < 640) && (CounterY < 480);
    end

    assign vga_h_sync = ~vga_HS;
    assign vga_v_sync = ~vga_VS;

    always @(posedge clock)
    begin
      if (inDisplayArea) begin
        add_color <= CounterY + CounterX + (639*CounterY);
        vga_out <= color; 
      end else begin// if it's not to display, go dark
        vga_out <= 3'b000;
        add_color <= 0;
        end
    end

endmodule