#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#define COLUMNS 7
#define ROWS 6

static void save_Color (uint32_t color, uint32_t pos) {
	//Se guardan el color, la posicion y se habilita la bandera de valido.
	*(( volatile uint32_t *)0x10000004 ) = pos;
	*(( volatile uint32_t *)0x10000000 ) = color;
	
	*(( volatile uint32_t *)0x10000008 ) = 1;
}

void draw_square(int x_centre, int y_centre, uint32_t color){
	int x = 0, y = 0, position = 0, x_position = 0, y_position = 0;		
	for (int y=0; y<60;y++){
		for (int x=0; x<63;x++){
		x_position = x + x_centre;
		y_position = y + y_centre;
		position = y_position + x_position + (639 * y_position);
		save_Color(color,position);
		}
	}
}

void draw_rectangle(int x_centre, int y_centre, uint32_t color){
	int x = 0, y = 0, position = 0, x_position = 0, y_position = 0;		
	for (y=5; y<30;y++){
		for (x=5; x<58;x++){
		x_position = x + x_centre;
		y_position = y + y_centre;
		position = y_position + x_position + (639 * y_position);
		save_Color(color,position);
		}
	}
}

int drop_ficha (int column, uint32_t board[ROWS][COLUMNS], uint32_t player_id) {
	for (int i = 6; i >= 0 ; i--) {
		if(board[i][column] == 0) {
			//Save dropped ficha as player 1
			board[i][column] = player_id;
			return 1;
		}
	}
	return 0;
}

uint32_t check_win (uint32_t board[ROWS][COLUMNS], uint32_t player_id) {
	//Horizontal case
	for(int i = 0; i < COLUMNS-3; i++){
		for(int j =0; j < ROWS; j++) {
			if(board[j][i] == player_id && board[j][i+1] == player_id && board[j][i+2] == player_id && board[j][i+3] == player_id) {
				return player_id;
			}
		}
	}
	//Vertical case
	for(int i = 0; i < COLUMNS; i++){
		for(int j =0; j < ROWS-3; j++) {
			if(board[j][i] == player_id && board[j+1][i] == player_id && board[j+2][i] == player_id && board[j+3][i] == player_id) {
				return player_id;
			}
		}
	}
	//Up diagonals
	for(int i = 0; i < COLUMNS-3; i++){
		for(int j =0; j < ROWS-3; j++) {
			if(board[j][i] == player_id && board[j+1][i+1] == player_id && board[j+2][i+2] == player_id && board[j+3][i+3] == player_id) {
				return player_id;
			}
		}
	}
	//Down diagonals
	for(int i = 0; i < COLUMNS-3; i++){
		for(int j =3; j < ROWS; j++) {
			if(board[j][i] == player_id && board[j-1][i+1] == player_id && board[j-2][i+2] == player_id && board[j-3][i+3] == player_id) {
				return player_id;
			}
		}
	}
	return 0;
}

int ai_check (uint32_t board[ROWS][COLUMNS]) {
	//the ai will check the places the player could win and will block the win
	uint32_t player_id = 1;
	for(int i = 0; i < COLUMNS-3; i++){
		for(int j =0; j < ROWS; j++) {
			//If 3 horizontal slots are found the ai will drop his slot on the next one
			if(board[j][i] == player_id && board[j][i+1] == player_id && board[j][i+2] == player_id) {
				drop_ficha(i+3, board, 2);
				return 1;
			}
		}
	}
	//Vertical case
	for(int i = 0; i < COLUMNS; i++){
		for(int j =0; j < ROWS-3; j++) {
			//If 3 vertical slots are found the ai will drop his slot on top of them
			if(board[j][i] == player_id && board[j+1][i] == player_id && board[j+2][i] == player_id) {
				drop_ficha(i, board, 2);
				return 1;
			}
		}
	}
	//Up diagonals
	for(int i = 0; i < COLUMNS-3; i++){
		for(int j =0; j < ROWS-3; j++) {
			if(board[j][i] == player_id && board[j+1][i+1] == player_id && board[j+2][i+2] == player_id) {
				drop_ficha(i+3, board, 2);
				return 1;
			}
		}
	}
	//Down diagonals
	for(int i = 0; i < COLUMNS-3; i++){
		for(int j =3; j < ROWS; j++) {
			if(board[j][i] == player_id && board[j-1][i+1] == player_id && board[j-2][i+2] == player_id) {
				drop_ficha(i+3, board, 2);
				return 1;
			}
		}
	}
	int random =0;
	//If no 3 slots are found in a row the ai will drop a slot as random, as long as there was space on that column
	while(!drop_ficha(random, board, 2)){
		random++;

	}
	return 1;

}

void main () {
	int enter = 0; 
	restart:
	if(enter){

	}
	int mid_game = false; 
	uint32_t rojo = 0xF00;
	uint32_t verde = 0x0F0;
	uint32_t naranja = 0xF40;
	uint32_t amarillo = 0xFF0;
	uint32_t azul = 0x006;
	uint32_t negro = 0x000;
	uint32_t blanco = 0xFFF;
	uint32_t position;
	uint32_t flag_half = 0;
	uint32_t width = 1;
	uint32_t input_column = 0;
	uint32_t current_player = 1;
	uint32_t board [ROWS][COLUMNS]; 
	uint32_t avail_columns = COLUMNS;
	int x_centre = 0, y_centre = 0;
	u_int32_t color = 0; 

	int drawn = 0;
	int fila=0,columna=0;
	int current_col =4;
	uint32_t boton_derecho = 0;
	uint32_t boton_izquierdo = 0;
	//fill board with 0
	for (int i = 0; i <6; i++) {
		for (int j =0; j <7; j++) {
			board[i][j] = 0;
		}
	}
	//WELCOME SCREEN PIXEL LOGIC HERE

	while(!mid_game) {
		//Se printea pantalla inicio
		if(!drawn) {
			for (int i=0; i < 7; i++){//columnas
				for (int j=0; j <6; j++){//filas
					x_centre = i*63;
					x_centre = x_centre + 98;

					y_centre = j*60; 
					y_centre = y_centre + 101;
					color=negro; 
					draw_square(x_centre,y_centre,color);
				}
			}
			int columnas[8];
			int filas[8];
			//lista de coordenadas x
			columnas[0] = 2;
			columnas[1] = 3;
			columnas[2] = 4;
			columnas[3] = 3;
			columnas[4] = 3;
			columnas[5] = 2;
			columnas[6] = 3;
			columnas[7] = 4;  
			//lista de coordenadas y
			filas[0] = 1;		
			filas[1] = 1;		 
			filas[2] = 1;		 
			filas[3] = 2;		 
			filas[4] = 3;		 
			filas[5] = 4;		 
			filas[6] = 4;		 
			filas[7] = 4;

			for (int i = 0; i < 8; i++){
				columna=columnas[i];
				fila=filas[i];
				x_centre = columna*63;
				x_centre = x_centre + 98;
				y_centre = fila*60; 
				y_centre = y_centre + 101;
				draw_square(x_centre,y_centre,verde);
			}
			drawn = 1; 
		}
		
		enter = *(( volatile uint32_t *)0x10000014);
	if(enter) {	
			//Boton enter presionado se dibuja el cuadro
			for (int i=0; i < 7; i++){//columnas
				for (int j=0; j <6; j++){//filas
					//con este for se recorren todos los cuadros del board y se dibujan uno por uno
					//board inicial colores blanco y azul intercalados
					x_centre = i*63;
					x_centre = x_centre + 98;

					y_centre = j*60; 
					y_centre = y_centre + 101;
					if (j%2 == 0){//fila par
						if (i%2 == 0){//columna par
							color = blanco;
						}
						else {//columna impar
							color = azul;
						}
					}
					else {//fila impar
						if (i%2 == 0){//columna par
							color = azul;
						}
						else {//columna impar
							color = blanco;
						}
					}
					draw_square(x_centre,y_centre,color);
				}
			}
			//se dibujan los indicadores de columna en blanco
			for (int i=0; i < 7; i++){//columnas
				int j=6;
				x_centre = i*63;
				x_centre = x_centre + 98;

				y_centre = j*60; 
				y_centre = y_centre + 101;

				color = blanco;
				draw_rectangle(x_centre,y_centre,color);
			}
			mid_game = true; 
			enter = 0;
	 	}
	}

	while(!check_win(board,1) && !check_win(board,2)) {
	// 	//PLAYER 1 LOGIC INPUT
		while(current_player == 1) {
			enter = 0;
			
			unavail_column:
			while(!enter) {
				enter = *(( volatile uint32_t *)0x10000014);
				boton_derecho = *(( volatile uint32_t *)0x10000010);
				boton_izquierdo = *(( volatile uint32_t *)0x10000018);
				if(boton_derecho) {
                    if(current_col == 6) {
                        current_col=0;
                    }
                    else {
                        current_col++;
                    }
                }
                if(boton_izquierdo) {
                    if(current_col == 0) {
                        current_col=6;
                    }
                    else {
                        current_col--;
                    }

                }
				//se actualizan los colores del selector de columna para que cambie respecto al contador current_col
				for (int sel_col=0; sel_col < 7; sel_col++){
						int sel_fila=6; 	//cte nos indica donde apuntar en el eje y
						
						x_centre = sel_col*63;  
						x_centre = x_centre + 98; //punto inicial en el eje x

						y_centre = sel_fila*60; 
						y_centre = y_centre + 101; //punto inicial en el eje y

						if (sel_col==current_col){//si sel_col=current_col se debe cambiar el color de la columna actual
							color=naranja;	
						}
						else{
							color = blanco;
						}
						draw_rectangle(x_centre,y_centre,color);
					}
				boton_derecho = 0;
				boton_izquierdo = 0;
			}
			int result = drop_ficha(current_col,board,1);
			 if(!result) {
			 		enter = 0;
			 		goto unavail_column; //If drop ficha fails means column was full 		
			 }
			 //Draw board here
				//Draw board here
            //  int check_win_result=0;
            //  check_win_result = check_win(board, 1);
            //  if(check_win_result==1) { //Check if player 1 won
            //      mid_game = 0; 
			// 	 break;
            //  } else {
            //      enter = 0;
            //      current_player = 1; //Change to player 2 after drawing works
            //  }
			 //Or draw here
			 for (int i=0; i < 7; i++){//columnas
				for (int j=0; j <6; j++){//filas
					//con este for se recorren todos los cuadros del board y se dibujan uno por uno
					//board inicial colores blanco y azul intercalados
					//siempre que cambie el valor de board[j][i] a algo diferente de 0 se repinta el cuadro.
					x_centre = i*63;
					x_centre = x_centre + 98;

					y_centre = j*60; 
					y_centre = y_centre + 101;
					
					if (board[j][i]==1){
						color=rojo;
					}
					else if (board[j][i]==2){
						color=amarillo;
					}
					else{
						if (j%2 == 0){//fila par
							if (i%2 == 0){//columna par
								color = blanco;
							}
							else {//columna impar
								color = azul;
							}
						}
						else {//fila impar
							if (i%2 == 0){//columna par
								color = azul;
							}
							else {//columna impar
								color = blanco;
							}
						}

					}
					draw_square(x_centre,y_centre,color);
				}
			}
			int check_win_result=0;
             check_win_result = check_win(board, 1);
             if(check_win_result==1) { //Check if player 1 won
                 mid_game = 0; 
				 break;
             } else {
                 enter = 0;
                 current_player = 2; //Change to player 2 after drawing works
             }
			mid_game = 0;
			//break;
		}
		while(current_player == 2) {
			 //enter = 0;
			ai_check(board);
			// unavail_column2:
			// while(!enter) {
			// 	enter = *(( volatile uint32_t *)0x10000014);
			// 	boton_derecho = *(( volatile uint32_t *)0x10000010);
			// 	boton_izquierdo = *(( volatile uint32_t *)0x10000018);
			// 	if(boton_derecho) {
            //         if(current_col == 6) {
            //             current_col=0;
            //         }
            //         else {
            //             current_col++;
            //         }
            //     }
            //     if(boton_izquierdo) {
            //         if(current_col == 0) {
            //             current_col=6;
            //         }
            //         else {
            //             current_col--;
            //         }

            //     }
			// 	//se actualizan los colores del selector de columna para que cambie respecto al contador current_col
			// 	for (int sel_col=0; sel_col < 7; sel_col++){
			// 			int sel_fila=6; 	//cte nos indica donde apuntar en el eje y
						
			// 			x_centre = sel_col*63;  
			// 			x_centre = x_centre + 98; //punto inicial en el eje x

			// 			y_centre = sel_fila*60; 
			// 			y_centre = y_centre + 101; //punto inicial en el eje y

			// 			if (sel_col==current_col){//si sel_col=current_col se debe cambiar el color de la columna actual
			// 				color=naranja;	
			// 			}
			// 			else{
			// 				color = blanco;
			// 			}
			// 			draw_rectangle(x_centre,y_centre,color);
			// 		}
			// 	boton_derecho = 0;
			// 	boton_izquierdo = 0;
			// }
			//  if(!drop_ficha(current_col, board, 2)) {
			//  		enter = 0;
			//  		goto unavail_column2; //If drop ficha fails means column was full 		
			//  }
			
			 //Or draw here
			 for (int i=0; i < 7; i++){//columnas
				for (int j=0; j <6; j++){//filas
					//con este for se recorren todos los cuadros del board y se dibujan uno por uno
					//board inicial colores blanco y azul intercalados
					//siempre que cambie el valor de board[j][i] a algo diferente de 0 se repinta el cuadro.
					x_centre = i*63;
					x_centre = x_centre + 98;

					y_centre = j*60; 
					y_centre = y_centre + 101;
					
					if (board[j][i]==1){
						color=rojo;
					}
					else if (board[j][i]==2){
						color=amarillo;
					}
					else{
						if (j%2 == 0){//fila par
							if (i%2 == 0){//columna par
								color = blanco;
							}
							else {//columna impar
								color = azul;
							}
						}
						else {//fila impar
							if (i%2 == 0){//columna par
								color = azul;
							}
							else {//columna impar
								color = blanco;
							}
						}

					}
					draw_square(x_centre,y_centre,color);
				}
			}
			int check_win_result=0;
             check_win_result = check_win(board, 2);
             if(check_win_result==2) { //Check if player 1 won
                 mid_game = 0; 
				 break;
				// break;
             } else {
                 enter = 0;
                 current_player = 1; //Change to player 2 after drawing works
             }
			mid_game = 0;
			//break;
		}






		*(( volatile uint32_t *)0x10000010 ) = 1;

	}
		if(check_win(board,1)) {
				int columnas[16];
				int filas[16];
				//lista de coordenadas x
				columnas[0] = 0;
				columnas[1] = 1;
				columnas[2] = 2;
				columnas[3] = 4;
				columnas[4] = 5;

				columnas[5] = 0;
				columnas[6] = 2;
				columnas[7] = 5;

				columnas[8] = 0;
				columnas[9] = 1;
				columnas[10] = 2;
				columnas[11] = 5;

				columnas[12] = 0;
				columnas[13] = 5;

				columnas[14] = 0;
				columnas[15] = 5;
				
				//lista de coordenadas y
				filas[0] = 1;		
				filas[1] = 1;		 
				filas[2] = 1;
				filas[3] = 1;		 
				filas[4] = 1;
						
				filas[5] = 2;		 
				filas[6] = 2;		 
				filas[7] = 2;

				filas[8] = 3;		 
				filas[9] = 3;
				filas[10] = 3;		 
				filas[11] = 3;

				filas[12] = 4;		 
				filas[13] = 4;

				filas[14] = 5;		 
				filas[15] = 5;

				for (int i = 0; i < 16; i++){
					columna=columnas[i];
					fila=filas[i];
					x_centre = columna*63;
					x_centre = x_centre + 98;
					y_centre = fila*60; 
					y_centre = y_centre + 101;
					draw_square(x_centre,y_centre,verde);
				}
		}

			if(check_win(board,2)) {
			//DIBUJAR P2 WINNER SCREEN 
				int columnas[21];
				int filas[21];
				//lista de coordenadas x
				columnas[0] = 0;
				columnas[1] = 1;
				columnas[2] = 2;
				columnas[3] = 4;
				columnas[4] = 5;
				columnas[5] = 6;

				columnas[6] = 0;
				columnas[7] = 2;
				columnas[8] = 6;
				
				columnas[9] = 0;
				columnas[10] = 1;
				columnas[11] = 2;
				columnas[12] = 4;
				columnas[13] = 5;
				columnas[14] = 6;
				
				columnas[15] = 0;
				columnas[16] = 4;

				columnas[17] = 0;
				columnas[18] = 4;
				columnas[19] = 5;
				columnas[20] = 6;
				
				//lista de coordenadas y
				filas[0] = 1;		
				filas[1] = 1;		 
				filas[2] = 1;
				filas[3] = 1;		 
				filas[4] = 1;		 
				filas[5] = 1;

				filas[6] = 2;		 
				filas[7] = 2;
				filas[8] = 2;	

				filas[9] = 3;
				filas[10] = 3;		 
				filas[11] = 3;
				filas[12] = 3;		 
				filas[13] = 3;
				filas[14] = 3;

				filas[15] = 4;
				filas[16] = 4;

				filas[17] = 5;		 
				filas[18] = 5;
				filas[19] = 5;		 
				filas[20] = 5;

				for (int i = 0; i < 21; i++){
					columna=columnas[i];
					fila=filas[i];
					x_centre = columna*63;
					x_centre = x_centre + 98;
					y_centre = fila*60; 
					y_centre = y_centre + 101;
					draw_square(x_centre,y_centre,verde);
				}
			}
	enter = 0;
	
	while(!enter) {
		enter = *(( volatile uint32_t *)0x10000014);
		if(enter) {
			goto restart;
		}
		
	}
}

